package com.fs.starfarer.api.impl.campaign.rulecmd;

import java.util.List;
import java.util.Map;

import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.campaign.RuleBasedDialog;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.rules.MemKeys;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.util.Misc.Token;
import data.world.RefugeeFleetInteractionDialogPluginImpl;

public class ICE_enterFleetMarket extends BaseCommandPlugin {

    @Override
    public boolean execute(String ruleId, InteractionDialogAPI dialog, List<Token> params, Map<String, MemoryAPI> memoryMap) {

        SectorEntityToken target = dialog.getInteractionTarget();

        if (target.getCustomInteractionDialogImageVisual() != null) {
            dialog.getVisualPanel().showImageVisual(target.getCustomInteractionDialogImageVisual());
        }
//        RefugeeFleetInteractionDialogPluginImpl.inConversation = false;
        RefugeeFleetInteractionDialogPluginImpl.needToForceEndBattle = true;
        if (dialog.getPlugin() instanceof RuleBasedDialog) {
            dialog.getInteractionTarget().setActivePerson(null);
            ((RuleBasedDialog) dialog.getPlugin()).notifyActivePersonChanged();
            MemoryAPI local = memoryMap.get(MemKeys.LOCAL);
            if (local != null && local.getBoolean("$hasMarket") && !local.contains("$menuState")) {
                FireBest.fire(null, dialog, memoryMap, "MarketPostOpen");
            } else {
                FireAll.fire(null, dialog, memoryMap, "PopulateOptions");
            }
        }
        return true;
    }

}
