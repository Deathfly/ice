package data.shipsystems;

import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipSystemAPI;
import com.fs.starfarer.api.plugins.ShipSystemStatsScript;

public class PhaseWarpStats implements ShipSystemStatsScript {

    @Override
    public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {
        ShipAPI ship = null;
        ShipSystemAPI cloak;
        if (stats.getEntity() instanceof ShipAPI) {
            ship = (ShipAPI) stats.getEntity();
            cloak = ship.getPhaseCloak();
            if (cloak == null) {
                cloak = ship.getSystem();
            }
            if (cloak == null) {
                return;
            }
        } else {
            return;
        }
        ship.setPhased(true);
        if (state == ShipSystemStatsScript.State.OUT) {
            stats.getMaxSpeed().unmodify(id); // to slow down ship to its regular top speed while powering drive down
        } else {
            stats.getMaxSpeed().modifyFlat(id, 250f * effectLevel);
            stats.getAcceleration().modifyFlat(id, 2000f * effectLevel);
            stats.getDeceleration().modifyFlat(id, 500f * effectLevel);
        }
    }

    @Override
    public void unapply(MutableShipStatsAPI stats, String id) {
        ShipAPI ship = null;
        if (stats.getEntity() instanceof ShipAPI) {
            ship = (ShipAPI) stats.getEntity();
        } else {
            return;
        }
        ship.setPhased(false);
        stats.getMaxSpeed().unmodify(id);
        //stats.getMaxTurnRate().unmodify(id);
        //stats.getTurnAcceleration().unmodify(id);
        stats.getAcceleration().unmodify(id);
        stats.getDeceleration().unmodify(id);
    }

    @Override
    public StatusData getStatusData(int index, State state, float effectLevel) {
        if (index == 0) {
            return new StatusData("increased speed", false);
        }
        return null;
    }
}
