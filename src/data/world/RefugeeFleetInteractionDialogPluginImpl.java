// rebuild for 0.7.2a by Deathfly
// found a nicer and easier way to make it work with rule + rulecmd in 0.7.2a
package data.world;


import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.BattleAPI.BattleSide;
import com.fs.starfarer.api.impl.campaign.FleetInteractionDialogPluginImpl;

public class RefugeeFleetInteractionDialogPluginImpl extends FleetInteractionDialogPluginImpl {

    public static boolean needToForceEndBattle = false;

    @Override
    public void advance(float amount) {
        if (needToForceEndBattle) {
            context.getBattle().leave(playerFleet, false);
            context.getBattle().finish(BattleSide.NO_JOIN, false);
            Global.getSector().getPlayerFleet().getFleetData().setSyncNeeded();
            Global.getSector().getPlayerFleet().getFleetData().syncIfNeeded();
            needToForceEndBattle = false;
        }
    }
}
