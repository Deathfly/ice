package data.ai.ship;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CombatEntityAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipCommand;
import com.fs.starfarer.api.combat.ShipwideAIFlags;
import com.fs.starfarer.api.combat.WeaponAPI;
import com.fs.starfarer.api.combat.WeaponGroupAPI;
import com.fs.starfarer.api.loading.WeaponGroupType;
import data.shipsystems.ai.EntropicInversionMatrixAI;
import data.tools.IceUtils;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;
import org.lazywizard.lazylib.combat.AIUtils;
import org.lwjgl.util.vector.Vector2f;

public class MeleeTempAI extends BaseShipAI {

    WeaponAPI tractorBeam;
    int bladeGroup;
    int tractorBeamGroup;
    EntropicInversionMatrixAI systemAI;
    boolean facingBetterTarget = false;
    MeleeAIFlags AIFlags = new MeleeAIFlags();
    String bladeID = "sun_ice_fissionblade";
    String blademiniID = "sun_ice_fissionblademini";

    void checkIfShouldDefend() {
        if (ship.getShield() != null && ship.getShield().isOff()) {
            ship.giveCommand(ShipCommand.TOGGLE_SHIELD_OR_PHASE_CLOAK, null, 0);
        }

        if (ship.getSystem().isActive()) {
            return;
        }

        boolean targetDeathEminent = ship.getShipTarget().getHitpoints()
                <= IceUtils.estimateIncomingDamage(ship.getShipTarget(), 1f);

        boolean targetMayDieSoon = targetDeathEminent
                || ship.getShipTarget().getHitpoints() <= 3000f;

        boolean canPhase = ship.getPhaseCloak() != null
                && ship.getFluxTracker().getMaxFlux() - ship.getFluxTracker().getCurrFlux()
                > ship.getPhaseCloak().getFluxPerUse() * 1.1f;

        float danger = IceUtils.estimateIncomingDamage(ship, 1)
                / (ship.getHitpoints() + ship.getMaxHitpoints());

        if (danger > 0.12f || targetDeathEminent) {
            boolean systemUsed = useSystem();
            if (!systemUsed && canPhase) {
                this.toggleDefenseSystem();
            } else if (!systemUsed) {
                vent();
            }
        } else if (danger > 0.03 && !targetMayDieSoon) {
            useSystem();
        }
    }

    @Override
    public void evaluateCircumstances() {
        if (ship.getFluxTracker().isOverloadedOrVenting()
                || !tractorBeam.isFiring()
                || ship.getFluxTracker().getFluxLevel() > 0.9f
                || (ship.getPhaseCloak() != null && ship.getPhaseCloak().isActive())
                || ship.getShipTarget() == null
                || ship.getShipTarget().getOwner() == ship.getOwner()
                || tractorBeam.getRange() < MathUtils.getDistance(ship.getShipTarget(), tractorBeam.getLocation())
                || !ship.getShipTarget().isAlive()
                || AIFlags.firendlyFire
                || !tractorBeam.isFiring()) {

            ship.resetDefaultAI();
        }

        checkIfShouldDefend();
    }

    public MeleeTempAI(ShipAPI ship, WeaponAPI tractorBeam) {
        super(ship);

        this.tractorBeam = tractorBeam;
        this.systemAI = new EntropicInversionMatrixAI();
        for (int i = 0; i < ship.getWeaponGroupsCopy().size(); i++) {
            WeaponGroupAPI wg = ship.getWeaponGroupsCopy().get(i);
            if (wg == ship.getWeaponGroupFor(tractorBeam)) {
                tractorBeamGroup = i;
                wg.toggleOn();
            }
            for (WeaponAPI w : wg.getWeaponsCopy()) {
                if (w.getId().startsWith(bladeID)) {
                    bladeGroup = i;
                    wg.toggleOff();
                    wg.setType(WeaponGroupType.LINKED);
                    break;
                }
            }
        }

        systemAI.init(ship, ship.getSystem(), null, Global.getCombatEngine());
        circumstanceEvaluationTimer.setInterval(0.1f);
        //SunUtils.print("Gobble time!");
    }

    @Override
    public void advance(float amount) {
        if (circumstanceEvaluationTimer.intervalElapsed()) {
            evaluateCircumstances();
        }

        systemAI.advance(amount, null, null, ship.getShipTarget());

        Vector2f lead = AIUtils.getBestInterceptPoint(ship.getLocation(), ship.getMaxSpeedWithoutBoost(), ship.getShipTarget().getLocation(), ship.getShipTarget().getVelocity());
        if(lead == null){
            lead = ship.getShipTarget().getLocation();
        }
        float angleToFace = VectorUtils.getAngle(ship.getLocation(), lead);

        if (Math.abs(MathUtils.getShortestRotation(ship.getFacing(), angleToFace)) < 15) {
            accelerate();
        }
        if (MathUtils.getDistance(ship, ship.getShipTarget()) <= 500) {
            strafeToward(lead);
        }
        turnToward(angleToFace);

        float dist = MathUtils.getDistance(ship, ship.getShipTarget());
        if (!AIFlags.firendlyFire && (dist < 100f || bladeGroup == tractorBeamGroup)) {
            selectWeaponGroup(bladeGroup);
            fireSelectedGroup(ship.getShipTarget().getLocation());
        }
    }

    // TODO - find a way around this disgusting hack.
    // DONE - by Deathfly, using AIFlags instead.
//    @Override
//    public boolean needsRefit() {
//        return false;
//    }
    @Override
    public ShipwideAIFlags getAIFlags() {
        return (ShipwideAIFlags) AIFlags;
    }

    public class MeleeAIFlags extends ShipwideAIFlags {

        public boolean firendlyFire = false;
        public CombatEntityAPI tractorBeam;
    }
}
